global randomness_source
load a4_randomness_source % Carica un file contenente un vettore considerato una sorgente random

global data_sets
temp = load('data_set');  % Importa il data set
data_sets = temp.data;

global report_calls_to_sample_bernoulli
report_calls_to_sample_bernoulli = false;

% Ritorna una matrice random 100x256 con valori da -1 a 1
% (rappresenta i pesi input-hidden di una RBM con 256 input e 100 hidden)
test_rbm_w = a4_rand([100, 256], 0) * 2 - 1;
% Ritorna una matrice random 100x256 con valori da -1 a 1
% (rappresenta pesi input-hidden di una RBM con 256 input e 10 hidden
small_test_rbm_w = a4_rand([10, 256], 0) * 2 - 1;

% estrae una cifra di input dal database, + il rispettivo target
temp = extract_mini_batch(data_sets.training, 1, 1);
% campiona un'immagine binaria sostituendo l'Intensità i di input di un dato pixel con un bianco o, con probabilità p(o) = i
data_1_case = sample_bernoulli(temp.inputs);

% estrae un batch di 10 cifre dal dataset
temp = extract_mini_batch(data_sets.training, 100, 10);
% campiona anche questo batch in un'immagine binaria
data_10_cases = sample_bernoulli(temp.inputs);
% temp = extract_mini_batch(data_sets.training, 200, 37);
% data_37_cases = sample_bernoulli(temp.inputs);

% usa come test degli stati nascosti un campionamento binario di un'immagine random
test_hidden_state_1_case = sample_bernoulli(a4_rand([100, 1], 0));
% usa come test degli stati nascosti un campionamento binario di un batch di 10 immagini random
test_hidden_state_10_cases = sample_bernoulli(a4_rand([100, 10], 1));
% test_hidden_state_37_cases = sample_bernoulli(a4_rand([100, 37], 2));

report_calls_to_sample_bernoulli = true;

clear temp;
