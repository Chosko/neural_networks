% This file was published on Wed Nov 14 20:48:30 2012, UTC.

% n_hid             - number of hidden units
% lr_rbm            - learning rate of RBM
% lr_classification - learning rate of backprop
% n_iterations      - number of iterations of RBM training
function R=a4_main(n_hid, lr_rbm, lr_classification, n_iterations)

% first, train the rbm
    global report_calls_to_sample_bernoulli
    report_calls_to_sample_bernoulli = false;
    global data_sets
    if prod(size(data_sets)) ~= 1,
        error('You must run a4_init before you do anything else.');
    end

    global rbm_w;



    %create and initialize RBM
    model_shape= [n_hid, 256];          % la forma del modello (cioè la matrice dei pesi della RBM: numero_hidden x numero_visible)
    training_data= data_sets.training;
    model = (a4_rand(model_shape, prod(model_shape)) * 2 - 1) * 0.1; % inizializza random il modello con pesi piccoli (da -0.1 a 0.1)

    momentum_speed = zeros(model_shape);  % usa un momentum per il training, inizializzato a 0
    mini_batch_size = 100;                % preleva gli input a batch di 100
    start_of_next_mini_batch = 1;         % indice dell'inizio del prossimo batch
    for iteration_number = 1:n_iterations
        mini_batch = extract_mini_batch(training_data, start_of_next_mini_batch, mini_batch_size);    % estrae il mini_batch dal training set
        start_of_next_mini_batch = mod(start_of_next_mini_batch + mini_batch_size, size(training_data.inputs, 2));  %salva l'indice del prossimo batch
        if start_of_next_mini_batch ==0
            start_of_next_mini_batch =1;
        end
        gradient = cd1(model, mini_batch.inputs);         % contrastive divergence per calcolare il gradiente delle log_probabilities degli input in base alla variazione dei pesi
        momentum_speed = 0.9 * momentum_speed + gradient; % calcolo il momento come .9 * momento al passo precedente + il gradiente appena calcolato
        model = model + momentum_speed * lr_rbm;          % aggiorno il modello sommandogli il momento * learning_rate
    end

    rbm_w= model; % imposto i pesi della RBM

    % rbm_w is now a weight matrix of <n_hid> by <number of visible units,
    % i.e. 256>. This is visualized in figure 1.
    show_rbm(rbm_w);



    %Up to here: result of training RBM.  Hidden units weights
    %visualized. Weights input hidden are those found by rbm:
    input_to_hid = rbm_w;


    hidden_representation = logistic(input_to_hid * data_sets.training.inputs); % Rappresenta i nodi hidden con le loro probabilità di essere attivati
    % train hid_to_class
    data_2.inputs = hidden_representation;
    data_2.targets = data_sets.training.targets;





     %Now backprop

    [A,B,R,err,c]=retropro(data_sets.training.inputs', data_sets.training.targets', n_hid,lr_classification, 0.01, n_iterations, 2525);



    [d1,p1]=comparer(R',data_sets.training.targets);

    figure(2)
    plot(err);
