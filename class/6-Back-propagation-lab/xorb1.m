xor1 = [
  0 0 0;
  0 1 1;
  1 0 1;
  1 1 0;
];

figure(1);
[A,B,R,err,c]=retroprostep(xor1(:,1:2),xor1(:,3),2,1,0,2000,1990);

[d,p]=comparer(round(R),xor1(:,3))

showstep(R,xor1(:,1:2),xor1(:,3));
