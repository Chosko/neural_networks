load con1;

% suppone caricato conc1 : c'� con1 in memoria, � 2500x3

% prima prova
% il problema � difficile, pongo errmax = .1
% aumento eta a 5. Neuroni nascosti sono 4, numero massimo di epoche 200.
% 1992 � il seme per l'inizializzazione casuale dei pesi.

%A sono i pesi da input a hidden, B quelli da hidden a output. R sono gli
%output ottenuti coi pesi appresi per tutti i pattern. err contiene gli
%errori quadratici medi per ogni epoca.  c � il numero di epoche in cui
%l'errore quadratico medio � > errmax.

[A,B,R,err,c]=retropro(con1(:,1:2),con1(:,3),4,1,.01,200,1990);

% calcolo errori d � il numero di righe diverse tra V e con1(:,3), p � la
% percentuale di quelle uguali.

[d,p]=comparer(round(R),con1(:,3))

V=round(R);



figure(1);
hold on;
for i=1:2500
   if V(i)>0
     plot(con1(i,1),con1(i,2),'or');
   else
      plot(con1(i,1),con1(i,2),'ob');
   end
end
hold off;


% pause
%
% % aumento il numero di neuroni nascosti a 20, abbasso err a 0.01. Provare
% % a fare le due cose separatamente. Provare a modificare learning rate.
%
% [A,B,R,err,c]=retropro(con1(:,1:2),con1(:,3),20,0.1,.001,300,1990);
%
%
% % la situazione � molto migliorata
% [d,p]=comparer(round(R),con1(:,3))
%
%
% V=round(R);
%
% figure(2);
% hold on;
% for i=1:2500
%    if V(i)>0
%       plot(con1(i,1),con1(i,2),'or');
%    else
%       plot(con1(i,1),con1(i,2),'ob');
%    end
% end
%
%

hold off;

%si visualizzi l'andamento dell'errore. Come cambia al cambiare del
%learning rate?
