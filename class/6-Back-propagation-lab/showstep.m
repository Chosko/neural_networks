function showstep(R,Inp,Targ)

[d,p] = comparer(round(R),Targ)

V=round(R);
C1 = V > 0;
C2 = V <= 0;
Inp1 = Inp(C1, :);
Inp2 = Inp(C2, :);

figure(1);
clf;
hold on;
plot(Inp1(:,1), Inp1(:,2), 'or');
plot(Inp2(:,1), Inp2(:,2), 'ob');
hold off;
