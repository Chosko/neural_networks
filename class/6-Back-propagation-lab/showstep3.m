function showstep3(R,Inp,Targ)

[d,p] = comparer(round(R),Targ)

V=round(R);
C1 = V > 0;
C2 = V <= 0;
Inp1 = Inp(C1, :);
Inp2 = Inp(C2, :);

figure(1);
clf;
axis equal
grid
view(13,12)
hold on;
plot3(Inp1(:,1), Inp1(:,2), Inp1(:,3), 'or');
plot3(Inp2(:,1), Inp2(:,2), Inp2(:,3), 'ob');
hold off;
