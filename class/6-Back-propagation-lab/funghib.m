
load fung
%
%presuppone che fung sia stato caricato e che ci sia in memoria la matrice fun 8124x127
%
% coppie di apprendimento
fun1i=fun(1:4062,1:125);
fun1t=fun(1:4062,126:127);
%
% coppie per il test
fun2i=fun(4063:8124,1:125);
fun2t=fun(4063:8124,126:127);
%
%apprendimento con 4 unit� nascoste
[A,B,R,err,c]=retropro(fun1i,fun1t,4,1,.004,200,1881);
%
% numero dei patterns non appresi, e percentuale di quelli appresi
[d,p]=comparer(round(R),fun1t)
%
% test sui dati mai visti dalla rete
R1=fr1(A,B,fun2i');

%errori
[d,p]=comparer(round(R1'),fun2t)








