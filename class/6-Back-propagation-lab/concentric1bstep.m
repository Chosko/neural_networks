load con1;

% suppone caricato conc1 : c'� con1 in memoria, � 2500x3

% prima prova
% il problema � difficile, pongo errmax = .1
% aumento eta a 5. Neuroni nascosti sono 4, numero massimo di epoche 200.
% 1992 � il seme per l'inizializzazione casuale dei pesi.

%A sono i pesi da input a hidden, B quelli da hidden a output. R sono gli
%output ottenuti coi pesi appresi per tutti i pattern. err contiene gli
%errori quadratici medi per ogni epoca.  c � il numero di epoche in cui
%l'errore quadratico medio � > errmax.

figure(1);
[A,B,R,err,c]=retroprostep(con1(:,1:2),con1(:,3),3,.1,.01,200,1990);

% calcolo errori d � il numero di righe diverse tra V e con1(:,3), p � la
% percentuale di quelle uguali.

[d,p]=comparer(round(R),con1(:,3))

showstep(R,con1(:,1:2),con1(:,3));
