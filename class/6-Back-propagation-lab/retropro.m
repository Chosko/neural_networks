function [A,B,R,err,c]=retropro(Inp,Targ,nhid,Eta,Err,Nmax,seed);

%
tic
%

[N,n]=size(Inp);
[N1,z]=size(Targ);
Inp= [Inp ones(N,1)];%Aggiungo 1 per il bias ad ogni pattern
n = n+1;%tengo conto del bias alle unit� hidden

if N~=N1
fprintf('Dimensionamento non corretto\n\n');
% break;
end

%Inizializzazione random dei pesi
rand('state',seed);

A=rand(nhid,n)-0.5;
B=rand(z,nhid+1)-0.5;%Tengo conto del bias a livello hidden



Inp=Inp';
Targ=Targ';
err=[];


c=0;
ciclo=0;

while ciclo==0
    Y=[f(A*Inp); ones(1, N)];%Output hidden pi� 1 per bias
    R=f(B*Y);
	q=(0.5*ones(1,z)*((R-Targ).^2))*ones(N,1)/N;
        err=[err q];


	if q<=Err | c>=Nmax
		ciclo=1;
	end

	if ciclo==0
		c=c+1;
		for k=1:N % per ogni pattern
			%
			% Modifica di A e B.
			%
			Yhid=f(A*Inp(:,k)); % Yhid è l'output del layer nascosto per il pattern corrente
			Out=f(B*[Yhid;1]);  % Out è l'output della rete per il pattern corrente

      % DOut è vettore che contiene in ogni sua componente il local gradient del rispettivo neurone di output
      % Local gradient di un neurone di output = e(k) * f'(netinput(k)).
      % e(k) è la differenza tra risposta desiderata e output
      %   cioè, per tutti i neuroni di output, Targ(:,k)-Out
      % f' è la derivata della funzione di attivazione. Essendo sigmoide, f'=f*(1-f)
      %   cioè, per tutti i neuroni di output, Out .* (1-Out)
			DOut=(Targ(:,k)-Out).*Out.*(1-Out);

			E=DOut'*B(:,1:nhid) % E è vettore contenente i local gradient di output pesati

      % DYhid è vettore che contiene in ogni componente il local gradient del rispettivo neurone nascosto
      % Local gradient di un neurone j nascosto = Sum_k(local_gradient_k * peso_kj) * f'(netinput_j)
      % E contiene già i local_gradient pesati, mentre Yhid.*(1-Yhid) è la derivata di f
			DYhid=E'.*Yhid.*(1-Yhid);
            B=B+Eta*DOut*[Yhid;1]';
			A=A+Eta*DYhid*Inp(:,k)';
			
		end
	end
end
R=R';
