%usaperc
%utilizza il percettrone dopo che i patterns sono stati appresi
%riceve W e una matrica di patterns
%restituisce una colonna di risposte, una per ogni pattern

function ris=usaperc(W,P); % Utilizza un percettrone già allenato. W è il vettore di pesi, P una matrice contenente i pattern sulle righe

[t,m]=size(P); %t patterns m dimensionali

P(:,m+1)=ones(t,1); % Aggiungo sull'ultima colonna gli input a 1 corrispondenti al bias

w=size(W,2); % controllo la dimensionalità dei pesi

if ne(w,m+1) % se la dimensionalità dei pesi (incluso bias) è diversa dalla dimensionalità dei pattern
   error('dimensioni errate!')
end

 ris=segno(W*P')'; %calcola il risultato
