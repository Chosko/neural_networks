%Implementazione un po' dozzinale del percettrone.
%Classifica un insieme di patterns in due classi.
%Gli inputs sono reali e gli outputs sono bipolari, 1,-1.
%Ci sono m valori di input pi� l'input del bias, posto sempre uguale a 1.
%Ci sono m+1 pesi che connettono gli inputs e il bias con l'unico output y.
%Gli m+1 pesi compongono un vettore W.
%Inizialmente W � casuale.
%La funzione di attivazione � il segno (con solo 1 e -1 come valori).
%In entrata viene data una matrice X (input) che ha m colonne, una per valore di input,
%e t righe, dove t � il numero dei patterns da apprendere.
%I patterns sono m-dimensionali.
%In ingresso viene dato anche il vettore colonna T (target) di t righe,
%che contiene i valori da apprendere (+1 o -1, che identificano le due classi).
%n � il numero di epoche fissate per l'apprendimento.
%Uso [a,b]=percett(X,T,n, lrate);
%a contiene la storia dei pesi e b la storia del numero di patterns appresi

% function [storiapesi,storiappresi]=percett(X,T,n,lrate);
% Esegue il learning algorithm su un percettrone.
% - X: Matrice del training set, dove le righe sono i pattern, e le colonne le componenti di input
% - T: Vettore colonna con le risposte desiderate per ogni input
% - n: numero di epoche fissate per l'apprendimento
% - lrate: il learning rate
function [W,iterazioni,storiappresi]=percett1(X,T,lrate);

[t,m]=size(X); % t è il numero totale di pattern, m la dimensionalità dell'input
W=rand(1,m+1); % inizializzazione casuale di W, vettore riga di pesi random, con un peso in più corrispondente al bias
X(:,m+1)=ones(t,1); % Aggiungo come ultima colonna del training set un m+1_esimo input a 1 che corrisponde al bias
storiappresi=zeros(1); % Storico del numero di pattern classificati correttamente. Una sola riga, una colonna per ogni epoca
n = 1;

while true % Itero finchè non è stato tutto classificato correttamente
    Y=segno(W*X'); % Calcolo l'output per tutti i pattern. Y è vettore riga
    storiappresi(n)=sum(T'==Y); % Calcolo il numero di pattern classificati correttamente e salvo nello storico
    if storiappresi(n) >= t
      break;
    end
    for j=1:t % Per ogni pattern
        if ne(Y(j),T(j))  % Se l'output è diverso dalla risposta desiderata
            W=W+lrate*T(j)*X(j,:); % Learning -> W(n+1) = W(n) + eta*X*T -> T permette di non dover usare l'IF
        end
    end
    n = n+1;
end
iterazioni = n-1;
