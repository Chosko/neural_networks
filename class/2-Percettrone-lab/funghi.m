load fung
%
%presuppone che fung sia stato caricato e che ci sia in memoria la matrice fun 8124x127
%
% coppie di apprendimento
fun1i=fun(1:4062,1:125); % Prendo i primi 4062 pattern 125-dimensionali come training set
fun1t=2*fun(1:4062,126)-1; % Prendo i primi 4062 valori dell'ultima colonna come risposta desiderata. Poi normalizzo tra 1 e -1 (anzichè 1 e 0)
%
% coppie per il test
fun2i=fun(4063:8124,1:125); % Prendo i pattern da 4063 a 8124 come pattern di teset
fun2t=2*fun(4063:8124,126)-1; % Prendo le rispettive risposte desiderate
%
rand('state',1992)  % Faccio in modo che rand restituisca sempre la stessa sequenza tra le varie esecuzioni di questo script
%
%apprendimento
[u,v]=percett(fun1i,fun1t,30, 2); % Apprendimento con 30 epoche e learning_rate=2. u e v sono la storiapesi e storiappresi
%
%v(length(v))
figure(1) % mostro finestra
plot(v)   % disegno la storia dei pattern appresi
fprintf('percentuale appresa correttamente : %f per cento \n',(max(v)/4062)*100) % Calcolo la percentuale di pattern appresi come il massimo valore di apprendimento della curva
%pause

[hh,h]=max(v); %indice del primo massimo della curva

W=u(:,h)'; %riga dei pesi ottimale (è il vettore pesi corrispondente al momento in cui la curva di apprendimento aveva il suo massimo)

%
%test

ris=usaperc(W,fun2i); % Utilizza il percettrone per classificare il set di test
z=sum(ris==fun2t) % calcolo il numero di pattern classificati correttamente
fprintf('risultati corretti su funghi mai visti : %f per cento \n',(z/4062)*100) % calcolo la percentuale di pattern classificati correttamente
