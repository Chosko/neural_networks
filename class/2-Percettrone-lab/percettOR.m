% OR problem
orProb = [
   1  1  1
   1  0  1;
   0  1  1;
   0  0  0;
];
%
%presuppone che fung sia stato caricato e che ci sia in memoria la matrice fun 8124x127
%
% coppie di apprendimento
or1i=orProb(:,1:2);
or1t=2*orProb(:,3)-1;

rand('state',1992)  % Faccio in modo che rand restituisca sempre la stessa sequenza tra le varie esecuzioni di questo script

%
%apprendimento
[W,iterazioni,v]=percett1(or1i,or1t,2); % Apprendimento con 30 epoche e learning_rate=2. u e v sono la storiapesi e storiappresi
%
%v(length(v))
figure(1) % mostro finestra
plot(v)   % disegno la storia dei pattern appresi
fprintf('Apprendimento eseguito in %d iterazioni\n', iterazioni);
fprintf('Pesi trovati: ');
display(W);
fprintf('percentuale appresa correttamente : %f per cento \n',(max(v)/4062)*100) % Calcolo la percentuale di pattern appresi come il massimo valore di apprendimento della curva
pause
bar(W);
