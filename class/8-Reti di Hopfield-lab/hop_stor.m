function W=hop_stor(P)

% function W=hop_stor(P)
%
%  performs the storage (learning phase) for a Hopfield network
%
%   W - weight matrix
%   P - patterns to be stored (column wise matrix)
%
% Hugh Pasika 1997

% Calcola i pesi applicando la Hebbian rule: w_ji = 1/M SUM_k(f_ki*f_kj)
% Dove
%   w_ji = w_ij = peso tra neurone i e neurone j -> W(j,i) = W(i,j)
%   M = numero di memorie fondamentali -> p (numero di colonne di P)
%   f_ki = componente i della memoria k -> sommatoria calcolata con dot(P(i,:),P(j,:))
[n p]=size(P) % dimensioni di P -> n = n. righe = n. pixel di una cifra = n. neuroni
W=zeros(n,n); % matrice dei pesi -> n righe e n colonne perchè ci sono n neuroni

% questi 2 cicli servono per confrontare tutte le possibili
% coppie non ordinate di pixel una volta sola
for j=2:n,
    for i=1:j-1,
        W(i,j)=dot(P(i,:),P(j,:))/p; % calcolo sommatoria / numero di memorie
        W(j,i)=W(i,j); % proprietà simmetrica dei pesi
    end
end
