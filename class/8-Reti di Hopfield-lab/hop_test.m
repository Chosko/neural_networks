function [s, count, M]=hop_test(W,x,update)

% function [s, count, M]=hop_test(W,x,update)
%
%   s - output state vector
%   count - number of cycles until stable state is reached
%   M - matrix containing the intermediate network states
%   W - weight matrix
%   x - probe vector
%   update - how often to view the network
%
% Hugh Pasika 1997

% initialize a few variables

[r c]=size(x);  % dimensioni del probe vector (righe per colonne di pixel dell'immagine)
s_prev=x(:);    % inizializzo lo stato precedente come il vettore di input serializzato
N=r*c;          % n. totale di pixel dell'immagine (dovrebbe essere 120) - non viene mai utilizzato!!!
count=0;        % n. di cicli
M=zeros(120,1); % Storico degli stati visitati inizializzato con una sola colonna (120 hardcoded?!?! perchè non N?)

% the next 5 lines determine if interim plotting is to be done
if update > 0,
    plot='on ';
else
    plot='off'; update=1; % set update to prevent divide by zero error later
end

% the upper limit in the loop is arbitrary (you'll never reach it)
while count < 1000,
    ind=1; clear ch;

  % store the network state
    	count=count+1;
  	M(:,count)=s_prev; % aggiorno lo storico degli stati visitati

  % ch vector will declare which neurons want to change
    for j=1:120, % Per ogni neurone (120 hardcoded ?!?! dovrebbe essere N)
        % calcola attivazione del neurone j come la
        % sommatoria dei neuroni i adiacenti (tutti) * i pesi W_ji.
        % La sommatoria è calcolata con il prodotto scalare
        nv(j)=sign(sum(W(j,:)'.*s_prev));

        % Se l'attivazione di j è != 0 ed è != dall'attivazione di j al passo
        % precedente, salvo l'indice j perchè è un neurone che vuole cambiare
        if nv(j)~=0 & abs(nv(j)-s_prev(j)) > 0, ch(ind)=j; ind=ind+1; end; % se l'attivazione != 0 ed è != dall'attivazione al passo precedente, segnala il
    end


  % now, do any neurons want to change? if no, break out of the loop
    if ind==1, break; end % condizione di uscita. Siamo in uno stato stabile e nessun neurone vuole cambiare
  % update one neuron
    r_ind=ceil(rand(1)*length(ch));  % Seleziono un indice random tra i neuroni che vogliono cambiare
    s_prev(ch(r_ind))=s_prev(ch(r_ind))*(-1);    % Aggiorna il neurone corrispondente invertendo il suo valore

  % the next 6 lines are plotting directives to show intermediate stages
    if (plot == 'on ') & floor(count/update) == (count/update) ; % aggiorna il disegno
	     hop_plotdig(s_prev,12,10,'Current State');
        fprintf(1,'\nThe current iteration is: %4.0f \n',count);
        fprintf(1,'Hit any key to continue. \n')
        pause
    end

end

% more plotting directives
if (plot == 'on '),
  hop_plotdig(s_prev,12,10,'Final State');
  fprintf(1,'\nTraining stopped after %4.0f iterations.\n',count);
end

% storage of stable state
M(:,count)=s_prev;      % Aggiungo lo stato stabile allo storico
s=reshape(s_prev,r,c);  % Lo stato stabile è quello dell'ultima iterazione
