package neurons

import "fmt"

// Acts as a synaptic link of a neuron.
func createSynapticLink(input chan float64, weight float64, output chan float64){
  go func(){
    for {
      select{
      case x, ok := <-input:
        if(!ok){ // Channel input is closed. close out and return
          fmt.Println("Synaptic link input closed -> closing output")
          close(output)
          return
        }
        fmt.Println(x, "-- * ", weight, "--> ", weight * x)
        output <- weight * x // Push to output and go back to listen
      }
    }
  }()
  return
}

// Acts as a computational node (sumout) of a neuron
func createComputationalNode(inputs []chan float64, output chan float64){
  inputClosedChan := make(chan bool)
  sumChan := make(chan float64)

  // This set of goroutines collect all inputs in sumChan
  for _,input := range inputs{
    go func(input chan float64){
      for {
        select{
          case x, ok := <-input:
            if(!ok){ // Channel input is closed. close out and return
              inputClosedChan <- true // tell one input is closed
              return
            }
            sumChan <- x // Push to output and go back to listen
        }
      }
    }(input)
  }

  // This goroutines read sumChan
  go func(){
    closedInputs := 0
    inputLen := len(inputs)
    collected := 0
    totalSum := float64(0)

    sendIfReady := func ()  {
      if collected + closedInputs >= inputLen{
        fmt.Println("_")
        fmt.Println(" \\")
        fmt.Println("-(+)-- Induced local field --> ", totalSum)
        fmt.Println("_/")
        output <- totalSum
        collected = 0
        totalSum = float64(0)
      }
    }

    for {
      select {
        case x := <-sumChan:
          totalSum += x
          collected ++
          sendIfReady()
        case <- inputClosedChan:
          closedInputs++
          if closedInputs < inputLen {
            sendIfReady()
          } else {
            fmt.Println("All computational node's inputs are closed -> closing output")
            close(output)
          }
      }
    }
  }()

  return
}

// Acts as an activation link of a neuron.
func createActivationLink(input chan float64, activation func(float64) float64, output chan float64){
  go func(){
    for {
      select{
      case x, ok := <-input:
        if(!ok){ // Channel input is closed. close out and return
          fmt.Println("Activation link input closed -> closing output")
          close(output)
          return
        }
        act := activation(x)
        fmt.Println(x, "-- phi(*) --> ", act)
        output <- act // Push to output and go back to listen
      }
    }
  }()
  return
}

func debugLink(input chan float64, msg string){
  go func(){
    for {
      select{
        case x, ok := <-input:
          if(!ok){ // Channel input is closed. return
            fmt.Println("Destroying link as input channel is closing")
            return
          }
          fmt.Println("[DEBUG] %s %v", msg, x)
      }
    }
  }()
  return
}

func debugLinks(input []chan float64, msg string){
  for _,in := range input {
    debugLink(in, msg);
  }
}
