package neurons

import (
  "math/rand"
)

// This is a neuron. It is private because it has to be instantiated
type neuron struct{
  w []float64                 // Weights
  x []chan float64            // Inputs
  wx []chan float64           // Weighted inputs
  v chan float64              // Induced local field
  d chan float64              // Desired output
  e chan float64              // Error signal
  phi func(float64) float64   // Activation function
  y chan float64              // out
  status string               // running status of the neuron
}

// Instantiate and return a new neuron
func NewNeuron(inputLen int, activation func(float64) float64) *neuron {
  n := new(neuron)
  n.w =  makeRandomArray(inputLen)  // Weights initialized to random values
  n.phi = activation                // Setup the activation function
  n.status = "stopped"              // Neuron starts in a stopped state
  return n;
}

func (n *neuron) changeStatus(to string) {
  if n.status == to{
    return
  }

  stop := func ()  {
    switch n.status {
      case "learning":
        n.stopLearning()
      case "classification":
        n.stopClassification()
    }
  }

  switch to {
    case "stopped":
      stop()
    case "learning":
      stop()
      n.setupLearning()
    case "classification":
      stop()
      n.setupClassification()
  }
  n.status = to
}

// Classify an example. To use when the neuron is already trained
func (n *neuron) Classify(inputs []float64) float64{
  // Input length check
  if len(inputs) != len(n.w){
    panic("Length of input array is wrong for this neuron")
  }

  n.changeStatus("classification")

  // Send inputs
  for i := range inputs {
    go func(i int, inputs []float64) {
      n.x[i] <- inputs[i]
    }(i,inputs)
  }

  // Wait for output and return
  out := <- n.y
  return out
}

func (n *neuron) stopLearning() {
  panic("not implemented")
  return
}

func (n *neuron) stopClassification() {
  for i := range n.x {
    close(n.x[i])
  }
  return
}

func (n *neuron) setupClassification(){
  n.x = makeSignalArray(len(n.w))     // Input channels
  n.wx = makeSignalArray(len(n.w))    // Weighted input channels
  n.v = make(chan float64)            // Induced loacl field channel
  n.y = make(chan float64)            // Output channel
  n.createSynapticLinks()
  n.createComputationalNode()
  n.createActivationLink()
}

func (n *neuron) setupLearning() {
  panic("not implemented")
  return
}

func (n *neuron) Learn() {
  panic("not implemented")
  return
}

func (n *neuron) createSynapticLinks(){
  for i := 0; i < len(n.x); i++{
    createSynapticLink(n.x[i], n.w[i], n.wx[i])
  }
  return
}

func (n * neuron) createComputationalNode(){
  createComputationalNode(n.wx, n.v)
  return
}

func (n * neuron) createActivationLink(){
  createActivationLink(n.v, n.phi, n.y)
  return
}

func makeSignalArray(len int) []chan float64 {
  arr := make([]chan float64, len)
  for i := range arr {
    arr[i] = make(chan float64)
  }
  return arr
}

func makeRandomArray(len int) []float64 {
  arr := make([]float64, len)
  for i := range arr{
    arr[i] = rand.Float64()
  }
  return arr
}
