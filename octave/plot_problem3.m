%% Copyright (C) 2016 Ruben Caliandro
%%
%% This program is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {@var{retval} =} plot_problem3 (@var{input1}, @var{input2})
%%
%% @seealso{}
%% @end deftypefn

%% Author: Ruben Caliandro <chosko@localhost.localdomain>
%% Created: 2016-09-04

% plot a 3d graph of the weights (in homogeneous coords) and the training_set
function plot_problem3(W, training_set)
    t1 = training_set(training_set(:, end) == -1, :);
    t2 = training_set(training_set(:, end) == 1, :);
    tx1 = t1(:,1);
    ty1 = t1(:,2);
    tz1 = tx1*0+1;
    tx2 = t2(:,1);
    ty2 = t2(:,2);
    tz2 = tx2*0+1;
    funcz = @(x,y,W) -(W(2)/W(1)).*y-(W(3)/W(1)).*x;
    funcy = @(x,W) -(W(1)/W(3)) -(W(2)/W(3)).*x;
    int_start = -1;
    int_end = 2;

    % Plot W homogeneous plane
    if W(1) ~= 0
      x = int_start:.2:int_end;
      y = int_start:.2:int_end;
      z = zeros(size(x,2),size(y,2));
      for i = 1:size(x,2)
        for j = 1:size(y,2)
          z(i,j) = funcz(x(i),y(j),W);
        end
      end
      surf(x,y,z);
      hold on
    end

    % Plot W line
    if W(3) ~= 0
      x = int_start:.2:int_end;
      y = funcy(x,W);
      z = x*0+1;
      plot3(x,y,z,'b-');
      axis([int_start int_end int_start int_end int_start int_end])
      hold on
    end

    plot3(tx1,ty1,tz1,'ro',tx2,ty2,tz2,'b*');
    draw_arrow([0 0 0], [W(2) W(3) W(1)]);
    axis([int_start int_end int_start int_end int_start int_end])
    hold off
  end
