function draw_arrow(vect1, vect2)
    if(size(vect1) ~= size(vect1))
        error('draw_arrow requires same vector size')
    end
    if(size(vect1,2) == 2)
        quiver( vect1(1),vect1(2),vect2(1)-vect1(1),vect2(2)-vect1(1),0, 'red' )
    end
    if(size(vect1,2) == 3)
        quiver3( vect1(1),vect1(2),vect1(3),vect2(1)-vect1(1),vect2(2)-vect1(2),vect2(3)-vect1(3),0,'red' )
    end
end