%% Copyright (C) 2016 Ruben Caliandro
%%
%% This program is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {@var{retval} =} algorithm (@var{input1}, @var{input2})
%%
%% @seealso{}
%% @end deftypefn

%% Author: Ruben Caliandro <chosko@localhost.localdomain>
%% Created: 2016-09-03

% Learning algorithm for a perceptron made with a single neuron,
function [W, updates] = learning_algorithm (training_set, learning_rate, debug_interval, W)

  if nargin < 4
    % Weights of the perceptron, random initialized
    W = 2 * rand(3,1) - 1;
  end
  if nargin < 3
    debug_interval = 0;
  end
  if nargin < 2
    learning_rate = .2;
  end
  if nargin < 1
    % Training set of the AND problem
    training_set = [
      1 1 1; % each sample is: x_1 x_2 expected_result
      1 0 -1;
      0 1 -1;
      0 0 -1;
    ];
  end

  function y = hard_limiter(x)
    if x > 0
      y = 1;
    else
      y = -1;
    end
  end

  phi = @(x) hard_limiter(x);

  % Learning rate
  eta = learning_rate;

  % used as a termination flag
  right_examples = 0;

  n=1;
  i=1;
  updates = 0;
  training_size = size(training_set,1);

  if debug_interval
    figure('units', 'normalized', 'position', [0 0 1 1])
  end

  % main loop
  while right_examples < training_size
      i = mod(n,training_size) + 1;
      X = [1; training_set(i,1:(end-1))']; % X = [(bias input = 1) sample_x1 sample_x2 ... sample_xn]
      class = phi(W' * X);
      if class == training_set(i,end)
        right_examples = right_examples+1;
      else
        if debug_interval
          subplot(1,2,1);
          plot_problem(W,training_set);
          subplot(1,2,2);
          plot_problem3(W,training_set);
          if(debug_interval > 0)
            pause(debug_interval)
          else if(debug_interval < 0)
            pause
              end
          end
        end
        W = W - class * eta * X;
        right_examples = 0;
        updates = updates+1;
      end
      n = n+1;
  end

  if debug_interval
    % plot results
    subplot(1,2,1);
    plot_problem(W,training_set);
    subplot(1,2,2);
    plot_problem3(W, training_set);
  end

end
