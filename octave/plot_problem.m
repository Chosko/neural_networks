%% Copyright (C) 2016 Ruben Caliandro
%%
%% This program is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% -*- texinfo -*-
%% @deftypefn {Function File} {@var{retval} =} plot_problem3 (@var{input1}, @var{input2})
%%
%% @seealso{}
%% @end deftypefn

%% Author: Ruben Caliandro <chosko@localhost.localdomain>
%% Created: 2016-09-04

% plot a 2d graph of the weights (in homogeneous coords) and the training_set
function plot_problem(W, training_set)
    t1 = training_set(training_set(:, end) == -1, :);
    t2 = training_set(training_set(:, end) == 1, :);
    tx1 = t1(:,1);
    ty1 = t1(:,2);
    tx2 = t2(:,1);
    ty2 = t2(:,2);
    funcy = @(x,W) -(W(1)/W(3)) -(W(2)/W(3)).*x;
    int_start = -1;
    int_end = 2;

    % Plot W line
    if W(3) ~= 0
      x = int_start:.2:int_end;
      y = funcy(x,W);
      plot(x,y,'b-');
      axis([int_start int_end int_start int_end]);
      hold on
    end

    plot(tx1,ty1,'ro',tx2,ty2,'b*');
    draw_arrow([0 0],[W(2) W(3)]);
    axis([int_start int_end int_start int_end]);
    hold off
  end
