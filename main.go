package main

import (
  "fmt";
  // "time";
  // "math/rand";
  "bitbucket.org/chosko/neural_networks/neurons"
)

func main() {
  // Example
  myExample := []float64 {1,1,1,1}

  // hard limiter
  activation := func (v float64) float64 {
    if(v > 0){
      return 1;
    }
    return -1;
  }

  // Instantiate neuron
  n := neurons.NewNeuron(len(myExample), activation)

  // Run example classification and read output
  out := n.Classify(myExample);

  fmt.Println("Input:", myExample, "Output:", out)
}
